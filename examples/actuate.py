#
# A sample HEM App to turn on and off nodes
# Author: Ashray Manur

import datetime
import threading
import time
import sys

sys.path.insert(1,'../')
from module.hemSuperClient import HemSuperClient

# Create a new HEM Client
hemSuperClient = HemSuperClient("localhost", 9931)

#This is function which gets triggered whenever you get data from server
def update(message, address):
	print 'Rec:', message, address
	#message is a list which gives you the type of response and the corresponding nodes 
	#address is a tuple giving you the server address and the port

#Subscribe to data from HEMs
# The argument to this is the name of the function you want triggered when you get data
hemSuperClient.subscribe(update)

def main():

	#Use this to send a request to server to turn on node 1
	#This can repeated for node 0-7
	hemSuperClient.sendRequest("/api/turnon/2")
	time.sleep(10)

	#Turn off node 3
	hemSuperClient.sendRequest("/api/turnoff/2")
	time.sleep(10)

if __name__ == "__main__":
    main() 

